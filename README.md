# Workshop: Introduction to CI/CD with Gitlab and Drupal 8 (4 hours)

* Date: Wed 27 November 2019
* Organiser: https://www.tomato-elephant-studio.com/

## Instructions

Full instructions available [here](http://bit.ly/ds19cicd).

## Prerequisites

1. [Register for workshop](https://drupalsouth.org/events/hobart-2019/workshops) at DrupalSouth website.

2. [Register account](https://gitlab.com/users/sign_in#register-pane) at Gitlab.com or [sign in](https://gitlab.com/users/sign_in) if you already have an account.

3. Head to to [gitlab repository](https://gitlab.com/testudio/workshop-cicd-drupal-gitlab) and fork it to your profile.

    - Click `fork` to fork the project
    
    ![Forking project](./resources/workshop-ci-cd-drupal-gitlab-prereq01a.png)
    
    - Select existing namespace for your project (most likely your username, e.g. `DrupalSouth` user)
    
    - You should be redirected to your own repo e.g. https://gitlab.com/DrupalSouth/workshop-cicd-drupal-gitlab (only example URL) with success message `The project was successfully forked.`


Clone your newly forked repository to you local machine.

![Cloning project](./resources/workshop-ci-cd-drupal-gitlab-prereq02a.png)

Make sure you have [git installed](https://docs.gitlab.com/ee/topics/git/how_to_install_git/) on your local machine.

Hello from Drupal South